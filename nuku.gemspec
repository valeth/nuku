# frozen_string_literal: true

lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require "nuku/version"

Gem::Specification.new do |spec|
  spec.required_ruby_version = ">= 2.6"

  spec.name = "nuku"
  spec.version = Nuku::VERSION
  spec.authors = "Patrick Auernig"
  spec.email = "dev.patrick.auernig@gmail.com"
  spec.summary = "Web extractor framework"
  spec.homepage = "https://gitlab.com/valeth/nuku"
  spec.license = "GPL-3.0"

  spec.files = Dir.chdir(File.expand_path("..", __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match?(%r{^(test|spec|features)/}) }
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "nokogiri", "~> 1.10", ">= 1.10.5"
  spec.add_dependency "mechanize", "~> 2.7"
  spec.add_dependency "addressable", "~> 2.6"
  spec.add_dependency "activesupport", "~> 5.2"
end
