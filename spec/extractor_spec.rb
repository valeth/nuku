# frozen_string_literal: true

require_relative "helpers/test_extractor"


RSpec.describe Nuku::Extractor do
  let(:doc) { test_fixture("test1") }
  let(:sel) { Nuku::Selector.new(doc) }

  it "can extract into a hash" do
    ext = Nuku::Extractors::TestExtractor.new(sel)

    expect( ext.wrapped? ).to be true

    expect( ext.extract )
      .to eq(test: { card_titles: ["Info", "Description"], second: "Lorem Ipsum" })
  end

  it "can define extractors on-the-fly" do
    ext = Nuku::Extractor.new(sel) do
      capture :article do
        find_by!(css: "section article").content
      end
    end

    expect( ext.wrapped? ).to be false

    expect( ext.extract )
      .to eq(article: ["Lorem Ipsum", "Dolor"])
  end

  it "raises an exception if no extractor is supplied" do
    expect { Nuku::Extractor.new(sel) }
      .to raise_exception(Nuku::InvalidExtractor)
  end

  it "can receive extra data to include in the output" do
    ext = Nuku::Extractor.new(sel, extra: { type: "pseudo latin" }) do
      capture :article do
        find_by!(css: "section article").content
      end
    end

    expect( ext.extract )
      .to eq(article: ["Lorem Ipsum", "Dolor"], type: "pseudo latin")
  end

  it "can receive external variables" do
    ext = Nuku::Extractor.new(sel, vars: { type: "pseudo latin" }) do |vars:, **|
      capture(:external) { vars[:type] }
    end

    expect( ext.extract )
      .to eq(external: "pseudo latin")
  end

  it "compacts Array and Hash results" do
    ext = Nuku::Extractor.new(sel, yields_value: true) do
      find_by!(css: "ul.first-list > li").content.map do |item|
        next if item.empty?
        item
      end
    end

    expect( ext.extract )
      .to eq(["First", "Second", "Third"])

    ext = Nuku::Extractor.new(sel) do
      capture(:first_list) do
        first_by(css: "ul.first-list > li.odd")&.content
      end

      capture(:second_list) do
        find_by!(css: "ul.second-list > li.odd").content
      end
    end

    expect( ext.extract )
      .to eq(second_list: ["1", "3", "5"])
  end

  it "can abort a procedure" do
    ext = Nuku::Extractor.new(sel) do
      capture(:one) { 1 }
      abort_procedure
      capture(:two) { 2 }
    end

    expect( ext.extract )
      .to eq(one: 1)

    ext = Nuku::Extractor.new(sel) do
      capture(:one) { 1 }
      abort_procedure!
      capture(:two) { 2 }

    expect( ext.extract )
      .to eq({})
    end
  end
end
