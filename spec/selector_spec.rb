# frozen_string_literal: true


RSpec.describe Nuku::Selector do
  let(:doc) { test_fixture("test0") }

  context "#find_by" do
    it "can select multiple elements" do
      sel = described_class.new(doc)

      expect( sel.find_by(css: "table > tr.odd > td.one") )
        .to be_a(described_class) & satisfy { |x| x.size == 2 }

      expect( sel.find_by(xpath: './/table/tr[@class="odd"]/td[@class="one"]') )
        .to be_a(described_class) & satisfy { |x| x.size == 2 }
    end

    it "returns empty selector on no match" do
      sel = described_class.new(doc)

      expect( sel.find_by(css: "table > tr.odd > td.four") )
        .to be_a(described_class) & be_empty

      expect( sel.find_by(xpath: './/table/tr[@class="odd"]/td[@class="four"]') )
        .to be_a(described_class) & be_empty
    end

    it "can map values over elements" do
      sel = described_class.new(doc)

      expect( sel.find_by(css: "table > tr.odd > td.one") { |x| x.size == 1 } )
        .to eq([true, true])

      expect( sel.find_by(css: "table > tr.odd > td.four") { |x| x.size == 1 } )
        .to eq([])
    end
  end

  context "#first_by" do
    it "can select a single element" do
      sel = described_class.new(doc)

      expect( sel.first_by(css: "table td.one") )
        .to be_a(described_class) & satisfy { |x| x.size == 1 }

      expect( sel.first_by(xpath: './/table//td[@class="one"]') )
        .to be_a(described_class) & satisfy { |x| x.size == 1 }
    end

    it "return nil on no match" do
      sel = described_class.new(doc)

      expect( sel.first_by(css: "table td.four") )
        .to be_nil

      expect( sel.first_by(xpath: './/table//td[@class="four"]') )
        .to be_nil
    end

    it "can map values over elements" do
      sel = described_class.new(doc)

      expect( sel.first_by(css: "table > tr.odd > td.one") { |x| x.size == 1 } )
        .to eq(true)

      expect( sel.first_by(css: "table td.four") { |x| x.size == 1 } )
        .to be_nil
    end
  end

  context "#content" do
    it "gets text content from selected nodes" do
      sel = described_class.new(doc)

      expect( sel.find_by(css: "table > tr.odd > td.one").content )
        .to eq(["One", "One"])

      expect( sel.find_by(css: "table > tr.odd > td.one").content(raw: true) )
        .to eq(["One  ", "  One"])

      expect( sel.first_by(css: "table > tr.odd > td.one").content )
        .to eq("One")

      expect( sel.first_by(css: "table > tr.odd > td.one").content(raw: true) )
        .to eq("One  ")

      expect( described_class.new([]).content )
        .to be_nil
    end
  end

  context "#attribute" do
    it "can get an attribute value" do
      sel = described_class.new(doc)

      expect( sel.find_by(css: "table > tr.odd td").attribute("class") )
        .to eq(["one", "two", "three", "one", "two", "three"])

      expect( sel.first_by(css: "table > tr.odd td").attribute("class") )
        .to eq("one")

      expect( sel.find_by(css: "table > tr.odd td").attribute("data-field") )
        .to eq([])

      expect( sel.first_by(css: "table > tr.odd td").attribute("data-field") )
        .to be_nil

      expect( described_class.new([]).attribute("class") )
        .to be_nil
    end
  end

  context "#find_by!" do
    it "raises exception on no match" do
      sel = described_class.new(doc)

      expect { sel.find_by!(css: "table > tr.odd > td.four") }
        .to raise_exception(Nuku::SelectionError)

      expect { sel.find_by!(xpath: './/table/tr[@class="odd"]/td[@class="four"]') }
        .to raise_exception(Nuku::SelectionError)
    end
  end

  context "#first_by!" do
    it "raises exception on no match" do
      sel = described_class.new(doc)

      expect { sel.first_by!(css: "table td.four") }
        .to raise_exception(Nuku::SelectionError)

      expect { sel.first_by!(xpath: './/table//td[@class="four"]') }
        .to raise_exception(Nuku::SelectionError)
    end
  end

  context "#content!" do
    it "raise exception if content is empty" do
      sel = described_class.new(doc)

      expect { sel.first_by(css: "table > tr.odd > td.three").content! }
        .to raise_exception(Nuku::SelectionError)

      expect( sel.find_by(css: "table > tr.odd > td.two").content! )
        .to eq(["", "Two"])

      expect { sel.find_by(css: "table > tr.odd > td.three").content! }
        .to raise_exception(Nuku::SelectionError)
    end
  end

  context "#attribute!" do
    it "raise exception if attribute is not found" do
      sel = described_class.new(doc)

      expect { sel.find_by(css: "table > tr.odd td").attribute!("data-field") }
        .to raise_exception(Nuku::SelectionError)

      expect { sel.first_by(css: "table > tr.odd td").attribute!("data-field") }
        .to raise_exception(Nuku::SelectionError)
    end
  end

  context "chaining" do
    it "returns the last result" do
      sel = described_class.new(doc)

      expect(
        sel.first_by(css: "body > table > tr")
          .find_by(xpath: './/td[@class="one"]').content
      ).to eq(["One"])

      expect(
        sel.find_by(css: "body > table > tr")
          .first_by(xpath: './/td[@class="one"]').content
      ).to eq("One")
    end
  end

  context "block nesting" do
    it "captures return values" do
      sel = described_class.new(doc)

      expect(
        sel.first_by(css: "body > table > tr") do |row|
          row.find_by(xpath: './/td[@class="one"]').content
        end
      ).to eq(["One"])

      expect(
        sel.find_by(css: "body > table > tr") do |rows|
          rows.find_by(xpath: './/td[@class="one"]').content
        end
      ).to eq([["One"], ["One"], ["One"]])
    end
  end
end
