# frozen_string_literal: true


RSpec.describe Nuku::Capture do
  let(:capture) { described_class.new }

  it "ignores nil values" do
    capture.scope(:top) { nil }

    expect(capture.to_h)
      .to eq({})
  end

  it "can clear the current state" do
    capture.scope(:top) { "something" }
    capture.clear

    expect(capture.to_h)
      .to eq({})
  end

  context "collections" do
    it "ignores empty values" do
      capture.scope(:top) { [] }
      capture.scope(:top) { {} }

      expect(capture.to_h)
        .to eq({})
    end

    it "combines scalar values as array" do
      capture.scope(:values) { 1 }
      capture.scope(:values) { 2 }

      expect(capture.to_h)
        .to eq(values: [1, 2])
    end

    it "joins arrays" do
      capture.scope(:values) { [1] }
      capture.scope(:values) { [2] }

      expect(capture.to_h)
        .to eq(values: [1, 2])
    end

    it "joins array and scalar value" do
      capture.scope(:values) { [1] }
      capture.scope(:values) { 2 }

      expect(capture.to_h)
        .to eq(values: [1, 2])
    end

    it "joins scalar value and array" do
      capture.scope(:values) { 1 }
      capture.scope(:values) { [2] }

      expect(capture.to_h)
        .to eq(values: [1, 2])
    end

    it "joins scalar value and hash as array" do
      capture.scope(:values) { 1 }
      capture.scope(:values) { { test: 2 } }

      expect(capture.to_h)
        .to eq(values: [1, {test: 2}])
    end

    it "joins hash and scalar value as array" do
      capture.scope(:values) { { test: 1 } }
      capture.scope(:values) { 2 }

      expect(capture.to_h)
        .to eq(values: [{test: 1}, 2])
    end

    it "merges hashes" do
      capture.scope(:values) { { test: 1 } }
      capture.scope(:values) { { test2: 2 } }

      expect(capture.to_h)
        .to eq(values: { test: 1, test2: 2 })
    end

    it "replaces identical hash keys" do
      capture.scope(:values) { { test: 1 } }
      capture.scope(:values) { { test: 2 } }

      expect(capture.to_h)
        .to eq(values: { test: 2 })
    end
  end

  context "nesting" do
    it "can nest capture scopes" do
      capture.scope(:outer) do
        capture.scope(:inner) do
          "value"
        end
      end

      expect(capture.to_h)
        .to eq(outer: { inner: "value" })
    end
  end

  context "scope options" do
    it "can allow empty values" do
      capture.scope(:values, allow_empty: true) { [] }
      capture.scope(:values) { {} }
      capture.scope(:values) { {} }
      capture.scope(:values) { nil }

      expect(capture.scope_options(:values))
        .to include(allow_empty: true)

      expect(capture.to_h)
        .to eq(values: [{}, {}])
    end

    it "can allow nil values" do
      capture.scope(:values, allow_nil: true) { nil }

      expect(capture.scope_options(:values))
        .to include(allow_nil: true)

      expect(capture.to_h)
        .to eq(values: nil)
    end

    it "can be forced to replace values" do
      capture.scope(:values) { 1 }
      capture.scope(:values, replace: true) { 2 }
      capture.scope(:values) { 3 }

      expect(capture.scope_options(:values))
        .to include(replace: true)

      expect(capture.to_h)
        .to eq(values: 3)

      capture.scope(:values, replace: false) { 4 }

      expect(capture.scope_options(:values))
        .to include(replace: false)

      expect(capture.to_h)
        .to eq(values: [3, 4])
    end

    it "can specify a default value" do
      capture.scope(:value, default: []) { nil }
      capture.scope(:mynil, default: nil) { [] }
      capture.scope(:string, default: "") { "actual value" }

      expect(capture.to_h)
        .to eq(value: [], mynil: nil, string: "actual value")

      capture.scope(:value) { "something" }

      expect(capture.to_h)
        .to eq(value: "something", mynil: nil, string: "actual value")
    end
  end
end
