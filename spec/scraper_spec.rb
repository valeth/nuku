# frozen_string_literal: true


RSpec.describe Nuku::Scraper, :vcr do
  it "receives HTML documents and wraps them in selectors" do
    scraper = described_class.new("https://duckduckgo.com/")

    document = scraper.scrape

    expect( document )
      .to be_a(Nuku::Selector)

    expect( document.first_by(css: "head > title").content )
      .to match(/^DuckDuckGo/)
  end

  it "raises exception if document not found" do
    scraper = described_class.new("https://valeth.info/_/this-will-never-exist.php")

    expect{ scraper.scrape }
      .to raise_exception(Nuku::NetworkError)
  end
end
