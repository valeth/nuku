# frozen_string_literal: true

require "bundler/setup"
require "nokogiri"
require "vcr"
require "nuku"


VCR.configure do |config|
  config.cassette_library_dir = "spec/cassettes"
  config.hook_into :webmock
  config.configure_rspec_metadata!
  config.default_cassette_options = {
    serialize_with: :compressed,
    record: :new_episodes
  }
end


RSpec.configure do |config|
  config.example_status_persistence_file_path = ".rspec_status"
  config.disable_monkey_patching!
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end


def test_fixture(name, type: :html)
  Nokogiri::HTML(open(File.expand_path("testdata/#{name}.#{type}", __dir__)))
end
