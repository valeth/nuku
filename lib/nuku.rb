# frozen_string_literal: true

require_relative "nuku/version"
require_relative "nuku/errors"
require_relative "nuku/extractor"
require_relative "nuku/scraper"


module Nuku
  include Errors
end
