# frozen_string_literal: true

require "mechanize"
require "addressable"

require_relative "errors"
require_relative "selector"


module Nuku
  module Errors
    class ScrapingError < NukuError
    end

    class NetworkError < ScrapingError
    end
  end


  class Scraper
    def initialize(template)
      @template = Addressable::Template.new(template).freeze
      @session = Mechanize.new
    end

    def scrape(**options)
      page_url = @template.expand(options)
      page = @session.get(page_url)
      selector = Selector.new(page&.root)
      block_given? ? yield(selector) : selector
    rescue Mechanize::ResponseCodeError => e
      raise Nuku::NetworkError, e
    rescue StandardError => e
      raise Nuku::ScrapingError, e
    end
  end
end
