# frozen_string_literal: true

module Nuku
  module Errors
    class NukuError < StandardError
    end
  end
end
