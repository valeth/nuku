# frozen_string_literal: true

require_relative "errors"


module Nuku
  module Errors
    class SelectionError < NukuError
    end

    class InvalidQuery < NukuError
    end
  end


  class Selector
    include Enumerable

    def initialize(elements)
      @elements = elements
    end

    # Select multiple matching elements
    #
    # @param css [String] Query with CSS selector
    # @param xpath [String] Query with Xpath selector
    # @return [Selector]
    # @return [Array<Object>] The result of the given block
    # @yieldparam sel [Selector]
    # @raise [Nuku::InvalidQuery]
    def find_by(css: nil, xpath: nil, &block)
      elems =
        if empty?
          []
        elsif css
          @elements.css(css)
        elsif xpath
          @elements.xpath(xpath)
        else
          raise Nuku::InvalidQuery, "No query specified, CSS or Xpath required"
        end

      selector = self.class.new(elems)
      block_given? ? selector.each(&block) : selector
    end

    # Select the first matching element
    #
    # @param css [String] Query with CSS selector
    # @param xpath [String] Query with Xpath selector
    # @return [Selector]
    # @return [Object] The result of the given block
    # @yieldparam sel [Selector]
    # @raise [Nuku::InvalidQuery]
    def first_by(css: nil, xpath: nil, &block)
      find_by(css: css, xpath: xpath, &block).first
    end

    # Get text content of selection
    #
    # @param raw [Bool] Wether to sanitize the result or not
    # @return [Array<String>, nil] If selection contains multiple elements
    # @return [String, nil] If selection contains single element
    def content(raw: false)
      items = extract_from_node(&:text)
      raw ? items : prepare_values(items)
    end

    # Get attributes of selection
    #
    # @param raw [Bool] Wether to sanitize the result or not
    # @return [Array<String>, nil] If selection contains multiple attributes
    # @return [String, nil] If selection contains one single attribute
    def attribute(name, raw: false)
      attrs = extract_from_node { |x| x.attr(name) }
      raw ? attrs : prepare_values(attrs)
    end

    def find_by!(css: nil, xpath: nil, &block)
      result = find_by(css: css, xpath: xpath, &block)
      raise Nuku::SelectionError if result.empty?
      result
    end

    def first_by!(css: nil, xpath: nil, &block)
      find_by!(css: css, xpath: xpath, &block).first
    end

    def content!(**options)
      result = content(**options)
      raise Nuku::SelectionError unless result_valid?(result)
      result
    end

    def attribute!(name, **options)
      result = attribute(name, **options)
      raise Nuku::SelectionError unless result_valid?(result)
      result
    end

    def each
      @elements.map do |element|
        yield self.class.new(element)
      end
    end

    def size
      case @elements
      when Nokogiri::XML::NodeSet then @elements.size
      when Nokogiri::XML::Node then 1
      when @elements.respond_to?(:size) then @elements.size
      else 0
      end
    end

    def empty?
      size.zero?
    end

  private

    def extract_from_node
      return if empty?

      if @elements.is_a?(Nokogiri::XML::NodeSet)
        @elements.map { |x| yield(x) }
      else
        yield(@elements)
      end
    end

    def prepare_values(items)
      case items
      when Array then items.map { |x| x&.chomp&.strip }.compact
      when String then items.chomp.strip
      else items
      end
    end

    def result_valid?(result)
      case result
      when Array then !result.all?(&:empty?)
      when String then !result.empty?
      when nil then false
      end
    end
  end
end
