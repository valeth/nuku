# frozen_string_literal: true

require "forwardable"

require_relative "errors"
require_relative "selector"
require_relative "capture"


module Nuku
  module Errors
    class InvalidExtractor < NukuError
    end

    class AbortProcedure < NukuError
    end
  end


  class Extractor
    extend Forwardable

    DEFAULT_OPTIONS = {
      wrap: false,
      yields_value: false,
      vars: {},
      extra: {}
    }.freeze

    BLOCK_ARGS = %i[
      vars
    ].freeze

    def_delegators :@selector, :find_by, :find_by!
    def_delegators :@selector, :first_by, :first_by!
    def_delegators :@selector, :attribute, :attribute!

    # @param selector [Nuku::Selector]
    # @param options [Hash]
    # @option options [Bool] flatten
    # @option options [Hash] vars
    # @option options [Hash] extra
    def initialize(selector, **options, &block)
      @selector = selector
      @memo = Capture.new

      @options = DEFAULT_OPTIONS.dup
        .merge(meta_variable(:@options) || {})
        .merge(options)

      @procedure = block if block_given?
      @procedure ||= meta_variable(:@procedure)

      raise Nuku::InvalidExtractor unless @procedure
    end

    # @return [Hash, Array]
    def extract
      options = @options.select { |k, _| BLOCK_ARGS.include?(k) }
      result = instance_exec(options, &@procedure)
      result = @options[:yields_value] ? result : @memo.to_h
      prepare_result(result)
    rescue Nuku::AbortProcedure
      @memo.to_h
    end

    # @return [String]
    def name
      extractor_name
    end

    def wrapped?
      @options[:wrap]
    end

  private

    def prepare_result(result)
      case result
      when Hash
        result.update(@options[:extra]).compact!
      when Array
        result.compact!
      end

      wrapped? ? { extractor_name.to_sym => result } : result
    end

    def meta_variable(name)
      self.class.instance_variable_get(name)
    end

    def class_name
      self.class.to_s.rpartition("::").last
    end

    def extractor_name
      class_name.delete_suffix("Extractor").downcase
    end

    # @param capture_name [String, Symbol]
    # @return [Hash]
    def capture(capture_name, **options)
      warn "#{class_name} yields value, captures will be ignored" if @options[:yields_value]

      @memo.scope(capture_name, options) do
        yield
      end
    end

    def abort_procedure
      raise Nuku::AbortProcedure
    end

    def abort_procedure!
      @memo.clear
      abort_procedure
    end

    class << self

      def extract(selector, **options, &block)
        new(selector, **options, &block).extract
      end

    private

      def set_option(**options)
        @options ||= {}
        @options.update(options)
      end

      def procedure(&block)
        @procedure = block
      end
    end
  end
end
