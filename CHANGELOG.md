# Changelog

All notable changes to this project will be documented in this file.


## [Unreleased]

*No changes yet*


---

The format is based on [Keep a Changelog], and this project adheres to [Semantic Versioning].


<!-- links -->

[Unreleased]: https://gitlab.com/valeth/nuku/tree/develop

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
